const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const Meme = require("./Meme.model");
require("dotenv").config(); // import this in order to make the environment variables visible

const port = process.env.PORT || 8081;

const app = express(); // instantiate the express app
app.use(bodyParser.json()); // middleware to be able to use req.body
app.use(cors()); // middleware to allow cross-origin requests

// Middleware to redirect from http to https in production environment
// Reference: https://jaketrent.com/post/https-redirect-node-heroku
// if (process.env.NODE_ENV === "production") {
//   app.use((req, res, next) => {
//     if (req.header("x-forwarded-proto") !== "https")
//       res.redirect(`https://${req.header("host")}${req.url}`);
//     else next();
//   });
// }

const db = process.env.MONGODB_URI || "mongodb://localhost/xmeme";

mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true }); // connect to mongodb

app.get("/", (req, res) => {
  // to test if the server is working correctly
  res.send("Hello World");
});

app.get("/memes", (req, res) => {
  // return the latest 100 memes
  Meme.find()
    .sort({ _id: -1 }) // the ID contains the timestamp too, so it's enough to sort based on the ID
    .limit(100)
    .select({ __id: 1, name: 1, url: 1, caption: 1 })
    .exec((err, memes) => {
      if (err) res.status(400).send(err);
      else {
        console.log("Latest 100 memes: ", memes);
        res.json(memes);
      }
    });
});
app.get("/memes/:id", (req, res) => {
  // return the meme for id
  const { id } = req.params;
  Meme.findOne({
    _id: id,
  })
    .select({ __id: 1, name: 1, url: 1, caption: 1 })
    .exec((err, meme) => {
      if (err) res.status(404).send(err);
      else {
        console.log("Meme: ", meme);
        res.json(meme);
      }
    });
});

app.post("/memes", (req, res) => {
  // add the meme to mongodb
  const { name, caption, url } = req.body;
  const newMeme = new Meme(); // create a meme object
  newMeme.name = name;
  newMeme.caption = caption;
  newMeme.url = url;

  newMeme.save((err, meme) => {
    if (err) res.status(400).send(err);
    else {
      console.log("Added: ", meme);
      res.json({
        id: meme._id,
      });
    }
  });
});

app.delete("/memes/:id", (req, res) => {
  // to delete a particular meme
  const { id } = req.params;
  Meme.findOneAndDelete({ _id: id }, (err, meme) => {
    if (err) res.status(400).send(err);
    else {
      console.log("Deleted: ", meme);
      res.json("Successfully deleted!");
    }
  });
});

app.patch("/memes/:id", (req, res) => {
  // update the meme with id 'id'
  const { id } = req.params;
  const { caption, url } = req.body;

  Meme.findOneAndUpdate({ _id: id }, { caption, url }, (err, meme) => {
    if (err) res.status(404).send(err);
    else {
      console.log("Updated: ", meme);
      res.json("Successfully updated!");
    }
  });
});

app.delete("/memes", (req, res) => {
  // to delete all the memes from the database
  Meme.deleteMany({}, (err, meme) => {
    if (err) res.status(400).send(err);
    else {
      console.log("Deleted: ", meme);
      res.json("Successfully deleted all memes!");
    }
  });
});

// make the server listen to a http port
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
